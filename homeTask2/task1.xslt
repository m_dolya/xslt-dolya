<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<xsl:variable name="substringText" select="'a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|.|:| '"/>
	<xsl:template match="/">
		<AllRooms>
			<xsl:apply-templates select="h:Houses/h:House">
				<xsl:sort select="@City"/>
				<xsl:sort select="number(translate(i:Address, $substringText, ''))"/>
			</xsl:apply-templates>
		</AllRooms>
	</xsl:template>
	<xsl:template match="h:House">
		<xsl:apply-templates select="h:Blocks/h:Block">
			<xsl:sort select="number(@number)"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="h:Block">
		<xsl:apply-templates select="r:Rooms/r:Room|r:Room">
			<xsl:sort select="number(@nuber)"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="r:Room">
		<Room>
			<Address>
				<xsl:value-of select="concat(translate(ancestor::h:House[1]/@City,$lowercase,$uppercase),'/',ancestor::h:House[1]/i:Address,'/',ancestor::h:Block[1]/@number,'/',@nuber)"/>
			</Address>
			<xsl:variable name="countRooms" select="count(ancestor::h:House[1]//r:Room)"/>
			<xsl:variable name="countGuests" select="sum(ancestor::h:House[1]//r:Room/@guests)"/>
			<HouseRoomsCount>
				<xsl:value-of select="$countRooms"/>
			</HouseRoomsCount>
			<BlockRoomsCount>
				<xsl:value-of select="count(ancestor::h:Block[1]//r:Room)"/>
			</BlockRoomsCount>
			<HouseGuestsCount>
				<xsl:value-of select="$countGuests"/>
			</HouseGuestsCount>
			<GuestsPerRoomAverage>
				<xsl:value-of select="floor(number($countGuests) div number($countRooms))"/>
			</GuestsPerRoomAverage>
			<Allocated>
				<xsl:choose>
					<xsl:when test="@guests = 1">
						<xsl:attribute name="Single">true</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="Single">false</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@guests = 2">
						<xsl:attribute name="Double">true</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="Double">false</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@guests = 3">
						<xsl:attribute name="Triple">true</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="Triple">false</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@guests = 4">
						<xsl:attribute name="Quarter">true</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="Quarter">false</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</Allocated>
		</Room>
	</xsl:template>
</xsl:stylesheet>

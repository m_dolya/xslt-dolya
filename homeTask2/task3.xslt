<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:s="xml_version_1.0" exclude-result-prefixes="s" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/s:Guests">
		<Peoples>
			<xsl:apply-templates select="s:Guest|end:Guest"/>
		</Peoples>
	</xsl:template>
	<xsl:template match="s:Guest|end:Guest">
		<People>
			<xsl:attribute name="AGE"><xsl:value-of select="@Age"/></xsl:attribute>
			<xsl:attribute name="NATIONALTY"><xsl:value-of select="@Nationalty"/></xsl:attribute>
			<xsl:attribute name="GENDER"><xsl:value-of select="@Gender"/></xsl:attribute>
			<xsl:attribute name="NAME"><xsl:value-of select="@Name"/></xsl:attribute>
			<xsl:if test="s:Type">
				<Type>
					<Text>
						<xsl:value-of select="s:Type"/>
					</Text>
				</Type>
			</xsl:if>
			<Profile>
				<xsl:if test="s:Profile/s:Address">
					<Address>
						<Text>
							<xsl:value-of select="s:Profile/s:Address"/>
						</Text>
					</Address>
				</xsl:if>
				<xsl:if test="s:Profile/s:Email">
					<Email>
						<Text>
							<xsl:value-of select="s:Profile/s:Email"/>
						</Text>
					</Email>
				</xsl:if>
			</Profile>
		</People>
	</xsl:template>
</xsl:stylesheet>

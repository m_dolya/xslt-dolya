<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<Guests>
			<xsl:for-each select="a">
				<Guest>
					<xsl:call-template name="main">
					</xsl:call-template>
				</Guest>
			</xsl:for-each>
		</Guests>
	</xsl:template>
	<xsl:template name="main">
		<xsl:param name="group-string" select="text()"/>
		<xsl:call-template name="string-division">
			<xsl:with-param name="string" select="$group-string"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="string-division">
		<xsl:param name="string"/>
		<xsl:param name="string-before" select="substring-before($string, '#')"/>
		<xsl:param name="string-after" select="substring-after($string, '#')"/>
		<xsl:choose>
			<xsl:when test="$string-after != ''">
				<Guest>
					<xsl:call-template name="guest">
						<xsl:with-param name="guest-string" select="$string-before"/>
					</xsl:call-template>
				</Guest>
			</xsl:when>
			<xsl:otherwise>
				<end:Guest>
					<xsl:call-template name="guest">
						<xsl:with-param name="guest-string" select="$string-before"/>
					</xsl:call-template>
				</end:Guest>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$string-after != ''">
			<xsl:call-template name="string-division">
				<xsl:with-param name="string" select="$string-after"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="guest">
		<xsl:param name="guest-string"/>
		<xsl:param name="type" select="substring-before($guest-string, '|')"/>
		<xsl:param name="age" select="substring-before(substring-after($guest-string,'|'), '|')"/>
		<xsl:param name="after-age" select="substring-after(substring-after($guest-string,'|'),'|')"/>
		<xsl:param name="nationalty" select="substring-before($after-age,'|')"/>
		<xsl:param name="after-nationalty" select="substring-after($after-age,'|')"/>
		<xsl:param name="gender" select="substring-before($after-nationalty, '|')"/>
		<xsl:param name="name" select="substring-after($after-nationalty,'|')"/>
		<xsl:attribute name="Age"><xsl:value-of select="$age"/></xsl:attribute>
		<xsl:attribute name="Nationalty"><xsl:value-of select="$nationalty"/></xsl:attribute>
		<xsl:attribute name="Gender"><xsl:value-of select="$gender"/></xsl:attribute>
		<xsl:attribute name="Name"><xsl:value-of select="$name"/></xsl:attribute>
		<Type>
			<xsl:value-of select="$type"/>
		</Type>
	</xsl:template>
</xsl:stylesheet>

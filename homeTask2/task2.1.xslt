<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:s="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<a>
		<xsl:for-each select="s:Guests/s:Guest|s:Guests/end:Guest">
			<xsl:variable name="group-string" select="concat(s:Type,'|',@Age,'|',@Nationalty,'|',@Gender,'|',@Name,'#')"/>
			<xsl:value-of select="$group-string"/>
		</xsl:for-each>
		</a>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:s="xml_version_1.0" exclude-result-prefixes="s" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/s:Guests">
		<result>
			<xsl:text>&#10;</xsl:text>
			<xsl:call-template name="beforeJimmy"/>
			<xsl:text>________________________</xsl:text>
			<xsl:text>&#10;</xsl:text>
			<xsl:call-template name="afterJimmy"/>
			<xsl:text>________________________</xsl:text>
			<xsl:text>&#10;</xsl:text>
			<xsl:call-template name="findAddress"/>
			<xsl:text>________________________</xsl:text>
			<xsl:text>&#10;</xsl:text>
			<xsl:call-template name="makeStr"/>
		</result>
	</xsl:template>
	<xsl:template name="beforeJimmy">
		<xsl:text>&#10;</xsl:text>
		<xsl:for-each select="s:Guest[not('Jimmy' = preceding-sibling::s:Guest/@Name)]">
			<xsl:variable name="names" select="@Name"/>
			<xsl:if test="not($names='Jimmy')">
				<xsl:value-of select="translate($names,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="afterJimmy">
		<xsl:text>&#10;</xsl:text>
		<xsl:for-each select="s:Guest[not('Jimmy' = following-sibling::s:Guest/@Name)]">
			<xsl:variable name="names" select="@Name"/>
			<xsl:variable name="nationalty" select="@Nationalty"/>
			<xsl:variable name="adress" select="s:Profile/s:Address"/>
			<xsl:if test="(not($names='Jimmy')) and (not($nationalty='BY'))">
				<xsl:value-of select="$adress"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="findAddress">
		<xsl:text>&#10;</xsl:text>
		<xsl:for-each select="s:Guest">
			<xsl:variable name="adress" select="s:Profile/s:Address"/>
			<xsl:if test="not(contains($adress,'Pushkinskaya'))">
				<Address>
					<xsl:value-of select="concat('Name=&quot;', @Name,'&quot;',' ','Nationality=&quot;',@Nationalty,'&quot;',' ')"/>
					<xsl:value-of select="$adress"/>
				</Address>
				<xsl:text>&#10;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="makeStr">
		<xsl:text>&#10;</xsl:text>
		<xsl:for-each select="s:Guest">
			<xsl:variable name="group-string" select="concat(s:Type,'|',@Age,'|',@Nationalty,'|',@Gender,'|',@Name,'#')"/>
			<xsl:value-of select="$group-string"/>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>

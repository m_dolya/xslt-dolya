<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:key name="GOODS_group" match="item" use="substring(@Name, 1, 1)"/>
	<xsl:template match="*|@*">
			<list>
				<xsl:apply-templates select="node()[generate-id(.)=generate-id(key('GOODS_group',substring(@Name, 1, 1)))]">
					<xsl:sort select="@Name"/>
				</xsl:apply-templates>
			</list>
	</xsl:template>
	<xsl:template match="item">
		<capital>
			<xsl:attribute name="value"><xsl:value-of select="substring(@Name, 1, 1)"/></xsl:attribute>
			<xsl:for-each select="key('GOODS_group',substring(@Name, 1, 1))">
				<xsl:sort select="@Name"/>
				<xsl:element name="name">
					<xsl:value-of select="@Name"/>
				</xsl:element>
			</xsl:for-each>
		</capital>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="*">
		Node 'foo' found <xsl:value-of select="count(//foo|//@foo)"/> times. 
		Node 'bar' found <xsl:value-of select="count(//bar|//@bar)"/> times. 			
	</xsl:template>
</xsl:stylesheet>

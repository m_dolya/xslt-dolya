
package com.soapserver.entities;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.soapserver.entities package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSFault_QNAME = new QName("http://soapserver.com/entities", "WSFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.soapserver.entities
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CitiesType }
     * 
     */
    public CitiesType createCitiesType() {
        return new CitiesType();
    }

    /**
     * Create an instance of {@link CountryResponse }
     * 
     */
    public CountryResponse createCountryResponse() {
        return new CountryResponse();
    }

    /**
     * Create an instance of {@link CountryType }
     * 
     */
    public CountryType createCountryType() {
        return new CountryType();
    }

    /**
     * Create an instance of {@link CountryRequest }
     * 
     */
    public CountryRequest createCountryRequest() {
        return new CountryRequest();
    }

    /**
     * Create an instance of {@link CitiesType.City }
     * 
     */
    public CitiesType.City createCitiesTypeCity() {
        return new CitiesType.City();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soapserver.com/entities", name = "WSFault")
    public JAXBElement<Object> createWSFault(Object value) {
        return new JAXBElement<Object>(_WSFault_QNAME, Object.class, null, value);
    }

}

package com.soapserver.entities;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryHotel", propOrder = {
        "country_name",
        "country_code",
        "hotel"
})

public class CountryHotel {

    @XmlElement(name = "Country_name", required = true)
    protected String country_name;

    @XmlElement(name = "Country_code", required = true)
    protected String country_code;

    @XmlElement(name = "Hotel" , required = true)
    protected List<HotelType> hotel;

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public List<HotelType> getHotel() {
        if (hotel == null) {
            hotel = new ArrayList<HotelType>();
        }
        return hotel;
    }

    public void setHotel(List<HotelType> hotel) {
        this.hotel = hotel;
    }
}

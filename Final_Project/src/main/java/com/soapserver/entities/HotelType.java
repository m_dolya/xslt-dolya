package com.soapserver.entities;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelType", propOrder = {
        "hotel_name",
        "hotel_code",
        "phone",
        "fax",
        "email",
        "url",
        "category_id",
        "check_in",
        "check_out",
        "latitude",
        "longitude"
})
public class HotelType {

    @XmlElement(name = "Hotel_name", required = true)
    protected String hotel_name;

    @XmlElement(name = "Hotel_code", required = true)
    protected String hotel_code;

    @XmlElement(name = "Phone", required = true)
    protected String phone;

    @XmlElement(name = "Fax", required = true)
    protected String fax;

    @XmlElement(name = "Email", required = true)
    protected String email;

    @XmlElement(name = "Url", required = true)
    protected String url;

    @XmlElement(name = "Category_id", required = true)
    protected String category_id;

    @XmlElement(name = "Check_in", required = true)
    protected String check_in;

    @XmlElement(name = "Check_out", required = true)
    protected String check_out;

    @XmlElement(name = "Latitude", required = true)
    protected String latitude;

    @XmlElement(name = "Longitude", required = true)
    protected String longitude;

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public String getHotel_code() {
        return hotel_code;
    }

    public void setHotel_code(String hotel_code) {
        this.hotel_code = hotel_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getCheck_out() {
        return check_out;
    }

    public void setCheck_out(String check_out) {
        this.check_out = check_out;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}

package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.CountriesDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

import java.util.List;

public class CheckHotelsFilter implements PreFilter<HotelsRequest> {

    private CountriesDAO countriesDAO;

    @Override
    public boolean isApplicable(final HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(final HotelsRequest request) throws ServiceException {
        if (request.getCityName() != null) {
            final List<String> countryNames = countriesDAO.getCityHotelsNames();
            if (!countryNames.contains(request.getCityName())) {
                throw new ServiceException("Requested city name is incorrect");
            }
        }
        if (request.getCode() != null){
            final List<String> codeList = countriesDAO.getCodeList();
            if (!codeList.contains(request.getCode())) {
                throw new ServiceException("Requested code country is incorrect");
            }
        }
        int hCategory = request.getHotelCategory();
        if ((hCategory < 1 || hCategory > 5) && hCategory != 0){
            throw new ServiceException("Hotel category must");
        }

    }

    public void setCountriesDAO(CountriesDAO countriesDAO) {
        this.countriesDAO = countriesDAO;
    }

}

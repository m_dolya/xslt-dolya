<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
	xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
	exclude-result-prefixes="ent sutil queryutil">
	
	<xsl:output method="xml"/>
	
	<xsl:template match="/">
		<ent:HotelsResponse>
			<ent:Country>
				<ent:Country_name>
					<xsl:value-of select="Result/Entry/country_name"/>
				</ent:Country_name>
				<ent:Country_code>
					<xsl:value-of select="Result/Entry/country_code"/>
				</ent:Country_code>
			<xsl:for-each select="Result/Entry">
					<ent:Hotel>
						<ent:Hotel_name>
							<xsl:value-of select="hotel_name"/>
						</ent:Hotel_name>
						<ent:Hotel_code>
							<xsl:value-of select="hotel_code"/>
						</ent:Hotel_code>
						<ent:Phone>
							<xsl:value-of select="phone"/>
						</ent:Phone>
						<ent:Fax>
							<xsl:value-of select="fax"/>
						</ent:Fax>
						<ent:Email>
							<xsl:value-of select="email"/>
						</ent:Email>
						<ent:Url>
							<xsl:value-of select="url"/>
						</ent:Url>
						<ent:Category_id>
							<xsl:value-of select="category_id"/>
						</ent:Category_id>
						<ent:Check_in>
							<xsl:value-of select="check_in"/>
						</ent:Check_in>
						<ent:Check_out>
							<xsl:value-of select="check_out"/>
						</ent:Check_out>
						<ent:Latitude>
							<xsl:value-of select="latitude"/>
						</ent:Latitude>
						<ent:Longitude>
							<xsl:value-of select="longitude"/>
						</ent:Longitude>
					</ent:Hotel>

			</xsl:for-each>
			</ent:Country>
		</ent:HotelsResponse>
	</xsl:template>
	
</xsl:stylesheet>
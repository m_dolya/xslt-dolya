<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
	exclude-result-prefixes="ent requtil">
	
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:variable name="code" select="ent:HotelsRequest/ent:Code"/>
		<xsl:variable name="language" select="ent:HotelsRequest/ent:Language"/>
		<xsl:variable name="cityName" select="ent:HotelsRequest/ent:CityName"/>
		<xsl:variable name="hotelName" select="ent:HotelsRequest/ent:HotelName"/>
		<xsl:variable name="hotelCategory" select="ent:HotelsRequest/ent:HotelCategory"/>

		<xsl:choose>
			<xsl:when test="$language != 'en' and $language != 'ru'">
				<xsl:text>Language not supported</xsl:text>
				<xsl:message>
					<xsl:value-of select="requtil:overrideResponse()"/>
				</xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>
SELECT
	ln.NAME AS country_name,
    l.CODE AS country_code,
    hn.NAME AS hotel_name,
    h.CODE AS hotel_code,
    h.PHONE AS phone,
    h.FAX AS fax,
    h.EMAIL AS email,
    h.URL AS url,
    h.CATEGORY_ID AS category_id,
    h.CHECK_IN AS check_in,
    h.CHECK_OUT AS check_out,
    h.LATITUDE AS latitude,
    h.LONGITUDE AS longitude
FROM
	gpt_location_name as ln INNER JOIN gpt_location as l
	  ON l.ID = ln.LOCATION_ID
    INNER JOIN gpt_hotel as h
	  ON l.ID = h.CITY_ID
    INNER JOIN gpt_hotel_name as hn
	  ON h.ID = hn.HOTEL_ID
    INNER JOIN gpt_language as lang
	  ON lang.ID = hn.LANG_ID
      AND lang.ID =  ln.LANG_ID
WHERE
	l.CODE = "</xsl:text>
				<xsl:value-of select="$code"/>
				<xsl:text>"
	AND lang.CODE ="</xsl:text>
				<xsl:value-of select="$language"/>
				<xsl:text>"
	AND ln.NAME = "</xsl:text>
				<xsl:value-of select="$cityName"/>
				<xsl:text>"</xsl:text>
				<xsl:if test="string-length($hotelName) &gt; 0">
					<xsl:text> AND hn.NAME LIKE "%</xsl:text>
					<xsl:value-of select="$hotelName"/>
					<xsl:text>%"</xsl:text>
				</xsl:if>
				<xsl:if test="$hotelCategory &gt; 0">
					<xsl:text> AND h.CATEGORY_ID = "</xsl:text>
					<xsl:value-of select="$hotelCategory"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>